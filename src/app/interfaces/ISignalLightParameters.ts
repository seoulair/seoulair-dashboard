export interface SignalLightParametersResult{
    name: string;
    stationCodes: string[];
    defaultColor: string;
}