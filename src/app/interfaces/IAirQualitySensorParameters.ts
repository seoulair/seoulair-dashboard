export interface AirQualitySensorParametersResult{
    name: string;
    readingDelayMs: number;
    dataPath: string;
}