export class AppResources{
    public static readonly apiUrl='http://localhost:5900/api/';

    public static readonly airQualityTurnOn='Device/AirQualitySensor/TurnOn';
    public static readonly airQualityTurnOff='Device/AirQualitySensor/TurnOff';
    public static readonly airQualityIsOn='Device/AirQualitySensor/IsOn';
    public static readonly airQualityParameters='Device/AirQualitySensor/parameters';
    public static readonly airQualityName='Device/AirQualitySensor/parameters/name/';
    public static readonly airQualityDelay='Device/AirQualitySensor/parametars/sendingDelayMs/';

    public static readonly signalLightTurnOn='Device/SignalLight/TurnOn/';
    public static readonly signalLightTurnOff='Device/SignalLight/TurnOff/';
    public static readonly signalLightIsOn='Device/SignalLight/IsOn/';
    public static readonly signalLightParameters='Device/SignalLight/parameters';
    public static readonly signalLightActiveColor='Device/SignalLight/ActiveColor/';
}