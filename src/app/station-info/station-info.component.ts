import {Component} from '@angular/core';

@Component({
    selector: 'station-info',
    templateUrl: 'station-info.component.html',
    styleUrls: ['station-info.component.scss']
})
export class StationInfoComponent {}